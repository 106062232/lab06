function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(){
            txtEmail.value = "";
            txtPassword = "" ;
            window.location.href='index.html';
        }).catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode == "auth/user-not-found") {
                txtEmail.value = "";
                create_alert("error", errorMessage);
            }
            else if (errorCode == "auth/wrong-password") {
                txtPassword.value = "";
                create_alert("error", errorMessage);
            }
        });
    });

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(){
            txtEmail.value = "";
            txtPassword = "" ;
            window.location.href='index.html';
        }).catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode == "auth/user-not-found") {
                txtEmail.value = "";
                create_alert("error", errorMessage);
            }
            else if (errorCode == "auth/wrong-password") {
                txtPassword.value = "";
                create_alert("error", errorMessage);
            }
        });
    });

    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(){
            txtEmail.value = "";
            txtPassword.value = "" ;
            create_alert("success", "Success creating account");
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode == "auth/email-already-in-use") {
                txtEmail.value = "";
                create_alert("error", errorMessage);
            }
            else if (errorCode == "auth/wrong-password") {
                txtPassword.value = "";
                create_alert("error", errorMessage);
            }
            else if (errorCode == "auth/weak-password") {
                txtPassword.value = "";
                create_alert("error", errorMessage);
            }
        });

    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};